#include<stdio.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
// this program is intended to check if a door is open.
//./program argument (ip) argument (port).
int main(int argc,char *argv[])
{
	if(argc!=3)
	{
		printf("Calistu\n");
		printf("Use ./socket [ip] [port]\n");
		exit(1);
	}
	int da;
	struct sockaddr_in addr;
	da=socket(AF_INET,SOCK_STREAM,6);
	if(da == -1)
	{
		printf("\nError in socket creation\n");
		exit(1);
	}
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[2]));
	addr.sin_addr.s_addr = inet_addr(argv[1]);
	if((inet_aton(argv[1], &addr.sin_addr))==0)
	{
		printf("\nThe ip is incorrect\n");
		exit(1);
	}
	if(connect(da,(struct sockaddr *) &addr,sizeof(addr))==-1)
	{
			printf("\nThe port is closed\n");
			exit(1);
	}
	printf("\nThe port is open\n");
}
